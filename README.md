# Ubuntu rust image

## Prerequisites
Have `docker` installed.

## How to run
First build the image if it is the first time

    docker build . -t rust-course

To run the image just use the following

    docker run -it --rm --entrypoint "/bin/bash" --name rust-course --mount type=bind,source="$(pwd)"/src,target=/app/rust-course/src rust-course:latest

You should see this output

    root@8b8df78caab8:/app/rust-course#

That means you're inside the docker container.

Try running `cargo run` to test if everything works correctly.

## Useful info
We're using a `bind mount` of the `src` folder. This means that all changes locally will reflect in the container so local development is possible.

Stops the container.

`docker stop rust-course`

Shows the container logs.

`docker logs rust-course`

Removes the container.

`docker rm rust-course`

Removes the container image. If you run this you will have to re run the `build` command.

`docker rmi rust-course`
